8 * 5 + 2

val answer = 8 * 5 + 2
answer
// answer = 43 // compiler error: cannot re-assign val

var response = 42
response = 43
response
// response = "hello" // compiler error: cannot change type

var greeting: String = null
greeting = "bye"

// In Scala everything is an object
1.to(10)

"Hello".intersect("World")

val x: BigInt = 123456789
x * x * x * x

1.to(10)
1 to 10 // equivalent to the line above

val result = 1.+(10)

"Hello".length

// Exercises
val a = 6 * 7
a
//a = 43 // error: reassignment to val

// val b; // error: val must have a value

val b: BigInt = 6 * 7
b.pow(1000)

import scala.math._
sqrt(10)

1.to(10)
1.to(10).map(sqrt(_))

6.*(7)

"Mississippi".distinct
"Rhine".permutations
"Rhine".permutations.toArray

"edu".permutations.toArray
"eduard".permutations.toArray
"janna".permutations.toArray
"arno".permutations.toArray
"lauri".permutations.toArray
"kristian".permutations.toArray
"karoliina".permutations.toArray

"ABC".sum
'A' + 'B' + 'C'
('A' + 'B' + 'C').toChar
'Æ'.toInt
'Æ'.toDouble
