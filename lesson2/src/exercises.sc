def isVowel(ch: Char) = "aeiou".contains(ch)

isVowel('a')
isVowel('b')

def isVowel2(ch: Char) = ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'

isVowel2('e')
isVowel2('c')

def vowels(s: String) = {
  var result = ""
  for (c <- s) {
    if (isVowel(c)) result += c
  }
  result
}

vowels("Nicaragua")

def vowels2(s: String) = for (c <- s; if isVowel(c)) yield c

vowels2("Nicaragua")

// recursive
def vowels3(s: String): String = {
  if (s.isEmpty) ""
  else {
    val ch = s(0)
    val rest = vowels3(s.substring(1))
    if (isVowel(ch)) ch + rest else rest
  }
}
vowels3("Nicaragua")

def vowels4(s: String) = {
  var i = 0
  var result = ""
  while(i < s.length) {
    val ch = s(i)
    if (isVowel(ch)) result += ch
    i += 1
  }
  result
}
vowels4("Nicaragua")

def vowels5(s: String, vowelChars: String = "aeiou", ignoreCase: Boolean = true) =
  for (ch <- if (ignoreCase) s.toLowerCase else s if vowelChars.contains(ch)) yield ch

vowels5("August")
vowels5("Überlätergehör", "aeiouäöü")

def vowels6(s: String, vowelChars: String = "aeiou", ignoreCase: Boolean = true): String =
  if (ignoreCase) vowels6(s.toLowerCase, vowelChars, false)
  else for (ch <- s if vowelChars.contains(ch)) yield ch

vowels6("August")
vowels6("Überlätergehör", "aeiouäöü", false)
vowels6("Überlätergehör", "aeiouäöü")
