val x = 4

if (x > 0) 1 else -1

val result = if (x > 0) 1 else -1
result

val result1 = if (x > 0) "positive" else -1
val result2 = if (x > 0) "positive"

val y = -4
val result3 = if (y > 0) "positive"

val result4 = ()

import scala.math._

val distance = {
  val dx = x - 0
  val dy = y - 0
  sqrt(dx * dx + dy * dy)
}

val n = 10
for (i <- 1 to n) println(i)

for (c <- "Hello") println(c)

for (i <- 1 to 3; j <- 1 to 3) print((10 * i + j) + " ")
println()

for (i <- 1 to 3; j <- 1 to 3; if i != j) print((10 * i + j) + " ")
println()

val result5 = for (i <- 1 to 10) yield i % 3
