// function
def abs(x: Double) = if (x >= 0) x else -x

// procedure or Unit function
def box(s: String) {
  val border = "-" * s.length + "--\n"
  println(border + "|" + s + "|\n" + border)
}
box("Hello")

// erroneous usage of unit function
def fac(n: Int) {
  var r = 1
  for (i <- 1 to n) r = r * i
  r
}
// last return does not return because function is of type Unit
val result = fac(10)

// to fix it add equal sign
def fac2(n: Int) = {
  var r = 1
  for (i <- 1 to n) r = r * i
  r
}
val result2 = fac2(10)

// default parameters
def decorate(str: String, left: String = "[", right: String = "]") = {
  left + str + right
}

decorate("Hello")

decorate("Hello", ">>>[")

// named parameters
decorate("Hello", right = "]<<<")

// varargs
def sum(args: Int*) = {
  var result = 0
  for (arg <- args) result += arg
  result
}
val s = sum(1, 4, 9, 16, 25)
val s2 = sum(1, 4, 9, 16, 25, 100)

sum(1 to 10 : _*)

def recursiveSum(args: Int*): Int = {
  if (args.isEmpty) 0
  else args.head + recursiveSum(args.tail : _*)
}
val s3 = recursiveSum(1, 4, 9, 16, 25)
