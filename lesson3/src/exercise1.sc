import scala.collection.mutable.ArrayBuffer

val buf = ArrayBuffer(1, 2, -3, 4, -5, 6, -7, 8)

// Java style code
def removeAllButFirstNegative(b: ArrayBuffer[Int]) {
  var first = true
  var i = 0
  while(i < b.length) {
    if (b(i) < 0) {
      if (first) {
        first = false
        i += 1
      } else {
        b.remove(i)
      }
    } else {
      i += 1
    }
  }
}

val indexes = for (i <- buf.indices if buf(i) < 0) yield i
val indexesToRemove = indexes.drop(1)
for (i <- indexesToRemove.reverse) buf.remove(i)
buf

def removeAllButFirstNegative2(buf: ArrayBuffer[Int]): Unit = {
  val indexes = for (i <- buf.indices if buf(i) < 0) yield i
  val indexesToRemove = indexes.drop(1)
  for (i <- indexesToRemove.reverse) buf.remove(i)
}

val buf1 = ArrayBuffer(1, 2, -3, 4, -5, 6, -7, 8)
removeAllButFirstNegative2(buf1)
buf1

def removeAllButFirstNegative3(buf: ArrayBuffer[Int]): Unit = {
  val indexesToRemove = (for (i <- buf.indices if buf(i) < 0) yield i).drop(1)
  for (i <- indexesToRemove.reverse) buf.remove(i)
}
val buf2 = ArrayBuffer(1, 2, -3, 4, -5, 6, -7, 8)
removeAllButFirstNegative3(buf2)
buf2

def removeAllButFirstNegative4(buf: ArrayBuffer[Int]) =
  for (i <- (for (i <- buf.indices if buf(i) < 0) yield i).drop(1).reverse) buf.remove(i)

val buf3 = ArrayBuffer(1, 2, -3, 4, -5, 6, -7, 8)
removeAllButFirstNegative4(buf3)
buf3

val buf4 = ArrayBuffer(1, 2, -3, 4, -5, 6, -7, 8)

for (i <- buf4.indices if !indexesToRemove.contains(i)) yield buf4(i)

def removeAllButFirstNegative5(buf: ArrayBuffer[Int]) = {
  val indexesToRemove = (for (i <- buf.indices if buf(i) < 0) yield i).drop(1)
  for (i <- buf.indices if !indexesToRemove.contains(i)) yield buf(i)
}
removeAllButFirstNegative5(buf4)
buf4
