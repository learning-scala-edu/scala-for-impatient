import java.io.File
import java.util.Scanner
// http://horstmann.com/presentations/livelessons-scala-2016/alice30.txt

//val in = new Scanner(new java.net.URL("http://horstmann.com/presentations/livelessons-scala-2016/alice30.txt").openStream)
val in = new Scanner(new File(scala.util.Properties.userHome + "/development/projects/scala/scala-for-impatient/lesson3/src/alice30.txt"))
val count = scala.collection.mutable.Map[String, Int]()
while (in.hasNext) {
  val word = in.next()
  count(word) = count.getOrElse(word, 0) + 1
}
count("Alice")
count("Rabbit")

val in2 = new Scanner(new File(scala.util.Properties.userHome + "/development/projects/scala/scala-for-impatient/lesson3/src/alice30.txt"))
var immutableCount = Map[String, Int]()
while(in2.hasNext) {
  val word = in2.next()
  immutableCount = immutableCount + (word -> (immutableCount.getOrElse(word, 0) + 1))
}
immutableCount("Alice")
immutableCount("Rabbit")
