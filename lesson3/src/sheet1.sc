// Arrays
val a = new Array[Int](10)
for (i <- 0 until a.length) a(i) = i
a
for (elem <- a) println(elem)

for (i <- a.indices) a(i) = i * i
a
for (elem <- a) println(elem)

import scala.collection.mutable.ArrayBuffer
val b = ArrayBuffer("Mary", "had", "a", "little", "lamb")

b += "its"
b += ("fleece", "was", "white")

b ++= Array("as", "snow")

b.remove(3)
b

b.insert(3, "medium-sized")
b

b.trimEnd(5)
b

val a1 = Array(2, 3, 5, 7, 11)
val result = for (elem <- a1) yield 2 * elem
val result1 = for (elem <- a1 if elem % 2 != 0) yield 2 * elem

Array(1, 7, 2, 9).sum
ArrayBuffer("Marry", "had", "a", "little", "lamb").max
ArrayBuffer(1, 7, 2, 9).sorted
Array(1, 7, 2, 9).reverse

val b1 = ArrayBuffer(1, 7, 2, 9)
val sortedB = b1.sorted
b1
sortedB
sortedB.reverse

Array(1, 2, 3).toString
Array(1, 2, 3).mkString(" | ")
b1.toString()

a.mkString(", ")
b.mkString("[", ", ", "]")
