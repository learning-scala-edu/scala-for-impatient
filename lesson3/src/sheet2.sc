// Maps and Tuples

val scores = Map("Alice" -> 21, "Arno" -> 26, "Lauri" -> 22, "Caroline" -> 17)

val mscores = scala.collection.mutable.Map("Alice" -> 21, "Arno" -> 26, "Lauri" -> 22, "Caroline" -> 17)

"Alice" -> 21

val arnoScores = scores("Arno")

// scores("Janna") // throws NoSuchElementException
// better to use .getOrElse() method
scores.getOrElse("Janna", 50)

// mscores("Alice") = 22 // for some reason this doesn't work in worksheet
mscores

mscores += ("Janna" -> 50)
mscores
mscores -= "Janna"
mscores

scores + ("Janna" -> 50)

val scores2 = scores + ("Janna" -> 50)
val scores3 = scores2 - "Janna"

for ((k, v) <- scores)
  println(k + " has score " + v)

for ((k, v) <- scores) yield (v, k)

scores.keySet
scores.values

val t = (1, 3.14, "Edu")
val pi = t._2
t._1
t _1
val (_, second, third) = t
