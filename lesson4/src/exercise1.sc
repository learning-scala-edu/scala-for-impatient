class Time(val hours: Int, val minutes: Int = 0) {
  if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59) throw new IllegalArgumentException
  def before(other: Time): Boolean = hours < other.hours || hours == other.hours && minutes < other.minutes
  override def toString: String = f"$hours%02d:$minutes%02d"
}

val morning = new Time(9, 0)
// val crazy = new Time(-1, 222) // throws IllegalArgumentException
val midnight = new Time(0)
val lastMinuteOfTheDay = new Time(23, 59)
val afternoon = new Time(16, 30)
morning.before(afternoon)
afternoon.before(morning)

val noon = new Time(12)
noon.hours
noon.minutes
