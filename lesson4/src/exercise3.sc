class Time(h: Int, m: Int = 0) {
  private var minutesSinceMidnight = h * 60 + m
  def hours = minutesSinceMidnight / 60
  def minutes = minutesSinceMidnight % 60
  def minutes_=(newValue: Int) {
    if (newValue < 0 || newValue > 59) {
      throw new IllegalArgumentException
    }
    minutesSinceMidnight = hours * 60 + newValue
  }
  if (h < 0 || h > 23 || m < 0 || m > 59) throw new IllegalArgumentException
  def -(other: Time) = hours * 60 + minutes - other.hours * 60 - other.minutes
  def before(other: Time): Boolean = minutesSinceMidnight < other.minutesSinceMidnight
  def <(other: Time): Boolean = this - other < 0
  def >(other: Time): Boolean = this - other > 0
  def ==(other: Time): Boolean = this - other == 0
  override def toString: String = f"$hours%02d:$minutes%02d"
}

object Time {
  def apply(h: Int, m: Int) = new Time(h, m)
}

val morning = new Time(9)
val afternoon = new Time(16, 30)
val noon = new Time(12)
morning.before(noon)
noon.before(afternoon)
afternoon.before(morning)

afternoon.hours
afternoon.minutes

noon
noon.minutes = 30
noon

Time(9, 0) - Time(12, 30)
Time(9, 0) < Time(12, 30)
Time(9, 0) > Time(12, 30)
Time(9, 0) == Time(12, 30)
Time(9, 0) == Time(9)
