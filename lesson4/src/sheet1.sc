class Point(val x: Double, val y: Double) {
  println(s"Welcome to ($x $y)")
  def this() { this(0, 0) }
  def move(dx: Double, dy: Double) = new Point(x + dx, y + dy)
  def distanceFromOrigin: Double = math.sqrt(x * x + y * y)
  override def toString: String = f"($x, $y)"
  def *(factor: Double) = new Point(x * factor, y * factor)
}

val p = new Point(3, 4)

p.move(10, 20)
p.distanceFromOrigin

p.x
p.y

class MutablePoint(var x: Double = 0, var y: Double = 0) {
  def move(dx: Double, dy: Double) = new Point(x + dx, y + dy)
  def distanceFromOrigin: Double = math.sqrt(x * x + y * y)
  override def toString: String = f"($x, $y)"
}
val p1 = new MutablePoint(3, 4)
p1.x = 23
p1.y = 14
p1

val p2 = new Point()
val p3 = new MutablePoint()

1 to 10 map (3 * _) filter (_ % 5 == 2)
1.to(10).map(3 * _).filter(_ % 5 == 2)

p * 2

1 :: 2 :: 3 :: Nil
