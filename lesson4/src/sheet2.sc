object Accounts {
  private var lastNumber = 0
  def newUniqueNumber() = { lastNumber += 1; lastNumber }
}

Accounts.newUniqueNumber()
Accounts.newUniqueNumber()
Accounts.newUniqueNumber()

class Point(val x: Double, val y: Double) {
  def this() { this(0, 0) }
  def move(dx: Double, dy: Double) = new Point(x + dx, y + dy)
  def distanceFromOrigin: Double = math.sqrt(x * x + y * y)
  override def toString: String = f"($x, $y)"
  def *(factor: Double) = new Point(x * factor, y * factor)
}
object Point {
  def apply(x: Double, y: Double) = new Point(x, y)
}

val p = Point(3, 4) * 5
