// Mixins with traits

trait Logged {
  def log(msg: String) {}
}

trait ConsoleLogger extends Logged {
  override def log(msg: String) { println(msg) }
}

class SavingsAccount extends Logged {
  private var balance: Double = 0
  def withdraw(amount: Double): Unit = {
    if (amount > balance) log("Insufficient funds")
    else balance -= amount
  }
}

val acc = new SavingsAccount with ConsoleLogger
acc.withdraw(1000)

trait TimestampLogger extends Logged {
  override def log(msg: String): Unit = {
    super.log(new java.util.Date() + " " + msg)
  }
}

trait ShortLogger extends Logged {
  val maxLength = 15
  override def log(msg: String): Unit = {
    super.log(
      if (msg.length <= maxLength) msg
      else msg.substring(0, maxLength - 3) + "..."
    )
  }
}

val acc1 = new SavingsAccount with ConsoleLogger with TimestampLogger with ShortLogger
acc1.withdraw(1000)

val acc2 = new SavingsAccount with ConsoleLogger with TimestampLogger with ShortLogger {
  override val maxLength: Int = 20
}
acc2.withdraw(1000)
